// const form = document.forms.resetData;
const mainForm = document.querySelector('.main-form>form');
const userContainer = document.querySelector('.user-container');

mainForm.addEventListener('submit', (evt) => {
    evt.preventDefault();
    const formData = new FormData(mainForm);
    const userName = formData.get('name');
    const lastName = formData.get('last-name');
    const age = formData.get('age');
    const data = formData.get('file');

    const mainBlockHtml = `
        <div class="main-block">
            <div class="user-info-block">
                <div class="initials">
                    <p>First name: <span class="user-name"> ${userName} </span></p>
                    <p>Last name: <span class="user-last-name"> ${lastName} </span></p>
                    <p>Age: <span class="user-age"> ${age} </span></p>
                </div>
                <div class="user-avatar">
                    <img src="https://www.practicepte.com/images/clients/avatar_man.jpg" alt="uaer-avatar">
                </div>
            </div>                
        </div> 
        `;
    userContainer.insertAdjacentHTML('beforeend', mainBlockHtml);
    const userAvatar = document.querySelector('.main-block:last-child>.user-info-block>.user-avatar>img');
    
    const file = new FileReader();
    console.log(file);

    file.onload = (ev) => {
        const { result } = ev.target; // прикольно что это синтаксис работает и здесь, я думал это доступно только в  react
        userAvatar.src = result;
    }

    file.readAsDataURL(data);
    mainForm.reset();
})